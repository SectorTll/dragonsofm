﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace dragonsOfMugloar.Helpers
{
    public class Request
    {
        /// <summary>
        /// Helper with retry pattern to put Json to server. Should be refactored
        /// </summary>
        /// <param name="address"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static string Put(string address,string json)
        {
            sbyte retry = 0;
            do
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        var content = new StringContent(json, Encoding.UTF8, "application/json");
                        var response =  client.PutAsync(address, content).Result;//TODO: have not time to resolve couple of race conditions bugs
                        var contents =  response.Content.ReadAsStringAsync().Result;
                        return contents;
                    }
                }
                catch (Exception ex)//TODO: extension
                {
                    LogManager.GetCurrentClassLogger().Debug("Get request exception: " + ex.Message);
                    retry++;
                    Thread.Sleep(1000);//TODO: semaphore 
                    if (retry > 3)
                    {
                        LogManager.GetCurrentClassLogger().Warn("Retry attempts is over: " + retry);
                        return null;
                    }
                }
            } while (true);
        }
        /// <summary>
        /// Helper Get with retry pattern,
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static string Get(string address)
        {
            LogManager.GetCurrentClassLogger().Debug("Get request: " + address);
            sbyte retry = 0;
            do
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.Timeout = new TimeSpan(0, 0, 3);
                        var responseString =  client.GetStringAsync(address).Result;
                        return responseString;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Debug("Get request exception: " + ex.Message);
                    retry++;
                    Thread.Sleep(1000);
                    if (retry > 3)
                    {
                        LogManager.GetCurrentClassLogger().Warn("Retry attempts is over: " + retry);
                        return null;
                    }
                        
                }
            } while (true);
        }
    }
}
