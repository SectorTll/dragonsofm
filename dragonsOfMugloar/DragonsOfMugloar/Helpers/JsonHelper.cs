﻿using Newtonsoft.Json;

namespace dragonsOfMugloar.Helpers
{
    /// <summary>
    /// JSON Helper
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Extension for game obeject mostly
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson(this object obj)
        {
           return JsonConvert.SerializeObject(obj,Formatting.Indented);
        }

        /// <summary>
        /// Creating object from Json string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T FromJson<T>(this string json)
        {
            return (T) JsonConvert.DeserializeObject(json,typeof(T));

        }
    }
}
