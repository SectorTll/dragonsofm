﻿using System.IO;
using System.Xml.Serialization;

namespace dragonsOfMugloar.Helpers
{
    public static class JXmlHelper
    {
        /// <summary>
        /// XMl extension, creating obj of from Xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T FromXml<T>(this string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xml))
            {
               return (T)serializer.Deserialize(reader);
            }
        }


    }
}
