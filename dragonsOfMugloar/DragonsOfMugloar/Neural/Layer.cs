﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Neural.Interfaces;
using NLog;

namespace dragonsOfMugloar.Neural
{
    /// <summary>
    /// For clarity, the items of Neural network divided into classes, Layer contains neurons 
    /// </summary>
    public class Layer : ILayer
    {
        public IDictionary<Guid, Neuron> Neurons { get; set; } = new Dictionary<Guid, Neuron>();//TODO: do i realy need guids?
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        public Layer(uint neuronsQuantity)
        {
            for (int i = 0; i < neuronsQuantity; i++)
            {
                Neuron neuron = new Neuron();
                AddNeuron(neuron);
            }
        }

        public void AddNeuron(Neuron neuron)
        {
            if (neuron.Id==Guid.Empty)
                neuron.Id=Guid.NewGuid();
            if (!Neurons.Keys.Contains(neuron.Id))
                Neurons.Add(neuron.Id,neuron);
            else
            {
               var ex= new ArgumentException("Neuron already present in the list.");
                _log.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Connect neurons of the child layer to synapses of the parent layer
        /// </summary>
        /// <param name="layer">child layer</param>
        public void ConnectLayer(Layer layer)
        {
            foreach (var neuron in Neurons.Values)
            {

                foreach (Neuron childNeuron in layer.Neurons.Values)
                {
                    Synapse synapse=new Synapse(childNeuron);
                    neuron.AddSynapse(synapse);
                }
                
            }
        }
        /// <summary>
        /// Calculate sigmoids of current layer
        /// </summary>
        public void Calculate()
        {
            foreach (var neuron in Neurons.Values)
            {
                neuron.Calculate();
            }
        }
    }
}
