﻿using System;
using System.Collections.Generic;

namespace dragonsOfMugloar.Neural.Interfaces
{
    public interface INeuron
    {
        IDictionary<Guid,Synapse> Synapses { get; set; }
    }
}