﻿using System;

namespace dragonsOfMugloar.Neural.Interfaces
{
    public interface ISynapse
    {
        Guid AxonId { get; set; }
        double Weight { get; set; }
    }
}