﻿using System;
using System.Collections.Generic;

namespace dragonsOfMugloar.Neural.Interfaces
{
    public interface ILayer
    {
        IDictionary<Guid,Neuron> Neurons { get; set; }
    }
}