﻿using System.Collections.Generic;

namespace dragonsOfMugloar.Neural.Interfaces
{
    public interface INeuronWeb
    {
        IList<Layer> Layers { get; set; }
    }
}