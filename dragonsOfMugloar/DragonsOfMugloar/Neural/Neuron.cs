﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Neural.Interfaces;
using Newtonsoft.Json;
using NLog;

namespace dragonsOfMugloar.Neural
{
    public enum NeuronState
    {
        Wrong=-1,
       Right=1
    }
    /// <summary>
    /// Class contains neuron logic,
    /// </summary>
    public class Neuron : INeuron
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public Guid Id;
        public String Name { get; set; }
        public IDictionary<Guid, Synapse> Synapses { get; set; } = new Dictionary<Guid, Synapse>();

        [JsonIgnore]
        public int Thrill { get; set; }

        public void AddSynapse(Synapse synapse)
        {
            //_logger.Debug("Synapse add ");
            if (synapse.Axon == null)
            {
                _logger.Error("Synapse axon is not set");
                throw new NullReferenceException("Synapse should be connected to neuron");
            }

            if (Synapses.ContainsKey(synapse.Axon.Id))
            {
                _logger.Error("Synapse axon id already present");
                throw new ArgumentOutOfRangeException("Synapse with same Axon already present with in the List: " +
                                                      synapse.Axon.Id);
            }
            Synapses.Add(synapse.Axon.Id,synapse);
        }
        /// <summary>
        /// Create synapses of current neuron to all neurons of input layer
        /// </summary>
        /// <param name="inpuLayer"></param>
        public void ConnectToLayer(Layer inpuLayer)
        {
            _logger.Debug("Connect to layer");
            _logger.Info("Connect to layer. Synapses before: " + Synapses.Count);
            Synapses.Clear();
            if (inpuLayer != null)
            foreach (var neuron in inpuLayer.Neurons.Values)
            {
                    Synapse synapse = new Synapse(neuron);
                    AddSynapse(synapse);
            }
        }
        /// <summary>
        /// Teach current neuron and all childs neurons
        /// </summary>
        /// <param name="state"></param>
        public void Teach( NeuronState state)//TODO: should be refactored in case of sig implementation
        {
            foreach (var synapse in Synapses.Values)
            {
                if (synapse.Axon.Thrill == 1)
                {
                    synapse.Weight = synapse.Weight + (int) state;
                    synapse.Axon.Teach(state);
                }
            }
        }
        /// <summary>
        /// Summator, calculate weights of neuron and set state
        /// </summary>
        /// <returns></returns>
        public double Calculate()
        {
            double sum = 0;// TODO: implement sig
            foreach (var synapse in Synapses.Values)
            {
                   sum = sum + synapse.Weight* synapse.Axon.Thrill;
            }
           
            //_logger.Debug("Neuron "+Id+" Sum: " +sum);
            Sum = sum;
            //double sig= 1 / (1 + Math.Exp(-NeuronWeb.Alpha * sum));
            //Thrill = sig;
            //_logger.Debug("Sig: " + sig);
            //return sig;
            return 0;
        }
        /// <summary>
        /// Summ of all weights
        /// </summary>
        [JsonIgnore]
        public double Sum { get; set; }
    }
}
