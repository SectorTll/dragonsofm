﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using dragonsOfMugloar.Neural.Interfaces;
using Newtonsoft.Json;

namespace dragonsOfMugloar.Neural
{
    /// <summary>
    /// Cont
    /// </summary>
    public class Synapse : ISynapse
    {
        private Neuron _axon;
        private Guid _axonId;
        public string Name;

        public Synapse(Neuron axon)
        {
            this._axon = axon;
            Weight = 0;
            if (axon!=null)
                Name = axon.Name; //Helper.GetRandom();
        }
        public double Weight { get; set; } = 0;
        /// <summary>
        /// hild neurons as input param
        /// </summary>
        [JsonIgnore]
        public Neuron Axon
        {
            get { return _axon; }
            set
            {
                _axonId = value.Id;
                _axon = value;
                Name = _axon.Name;
            }
        }
        /// <summary>
        /// Child neurons id
        /// </summary>
        public Guid AxonId
        {
            get
            {
                if (_axonId==Guid.Empty && _axon != null)
                {
                    _axonId = _axon.Id;
                }
                   
                return _axonId;
            }
            set { _axonId = value; }
        }
    }
}
