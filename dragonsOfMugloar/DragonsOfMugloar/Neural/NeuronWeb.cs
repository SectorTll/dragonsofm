﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Helpers;
using dragonsOfMugloar.Neural.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using NLog;

namespace dragonsOfMugloar.Neural
{
    /// <summary>
    /// Contains all layers of current perceptron
    /// </summary>
    public class NeuronWeb : INeuronWeb
    {
        /// <summary>
        /// Correct value for sigmoid function
        /// </summary>
        public static double Alpha = 1;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public IList<Layer> Layers { get; set; } = new List<Layer>();

        public string Name;

        public void AddLayer(Layer layer)
        {
            _logger.Debug("Insert layer, count of layers: " + Layers.Count);

            if (Layers.Count != 0)
            {
                var prevLayer = Layers[Layers.Count - 1];
                layer.ConnectLayer(prevLayer);
            }
            Layers.Add(layer);
        }
        /// <summary>
        /// Save current neural network to file
        /// </summary>
        /// <param name="path"></param>
        public void Save(String path)
        {
            try
            {
                var t = this.ToJson();
                using (StreamWriter sw = new StreamWriter(path))
                {
                    using (JsonWriter writer = new JsonTextWriter(sw))
                    {
                        writer.WriteRaw(t);
                    }

                }
            }
            catch (IOException ex)
            {
               _logger.Error(ex);
            }
           
        }
        /// <summary>
        /// Create neural network from file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static NeuronWeb Load(String path)
        {
            try
            {
                Dictionary<Guid, Neuron> temp = new Dictionary<Guid, Neuron>();
                using (StreamReader sr = new StreamReader(path))
                {
                    var json = sr.ReadToEnd();
                    NeuronWeb t = (NeuronWeb)JsonConvert.DeserializeObject(json, typeof(NeuronWeb));
                    foreach (var layer in t.Layers)
                    {
                        foreach (var neuron in layer.Neurons)
                        {
                            temp.Add(neuron.Key, neuron.Value);
                        }

                    }
                    foreach (var layer in t.Layers)
                    {
                        foreach (var neuron in layer.Neurons.Values)
                        {
                            foreach (var synapse in neuron.Synapses.Values)
                            {
                                synapse.Axon = temp[synapse.AxonId];
                            }
                        }
                    }
                    return t;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error("Unable to load NeuralWeb file " +ex.Message);
            }
            return null;
        }
        /// <summary>
        /// Start calculation of all weight of all neurons
        /// </summary>
        /// <returns></returns>
        public Neuron Calculate()
        {
            for (int i = 1; i < Layers.Count; i++)
            {
                Layers[i].Calculate();
            }
            Neuron winner = null;
           
            foreach (var neuron in Layers[Layers.Count-1].Neurons.Values)
            {
                //if (neuron.Thrill)
                {
                    if (winner == null)
                        winner = neuron;
                    else
                    {
                        if (winner.Sum < neuron.Sum)
                            winner = neuron;
                    }
                }
            }
            return winner;
        }
    }
}
