﻿using System;
using System.Threading.Tasks;
using dragonsOfMugloar.Game.Builders;

namespace DragonsOfMugloar.Game
{
    /// <summary>
    /// Class contains strategy for creating all gane persons. Builder pattern
    /// </summary>
    public class Director
    {
        /// <summary>
        /// Strategy for creating IHero persons
        /// </summary>
        /// <param name="builder">Builder of conc hero</param>
        /// <returns></returns>
        public IHero Construct(Builder builder)
        {
            builder.GetStringFromWeb();
            return builder.GetHeroFromString();
        }
    }
}