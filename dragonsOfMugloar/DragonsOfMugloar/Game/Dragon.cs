﻿namespace DragonsOfMugloar.Game
{
   
    /// <summary>
    /// Class created by Json to Class Paste
    /// </summary>
    public class Rootobject
    {
        public Dragon dragon { get; set; }
    }
    /// <summary>
    /// Contains drgons values
    /// </summary>
    public class Dragon
    {
        public int scaleThickness { get; set; }
        public int clawSharpness { get; set; }
        public int wingStrength { get; set; }
        public int fireBreath { get; set; }
        
        /// <summary>
        /// Create new dragon with same values
        /// </summary>
        /// <returns>Dragon</returns>
        public Dragon Copy()
        {
            Dragon copy = new Dragon
            {
                clawSharpness = this.clawSharpness,
                fireBreath = this.fireBreath,
                scaleThickness = this.scaleThickness,
                wingStrength = this.wingStrength
            };
            return copy;
        }
    }

}
