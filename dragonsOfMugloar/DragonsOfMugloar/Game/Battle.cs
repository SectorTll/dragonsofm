﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Game.Builders;
using dragonsOfMugloar.Helpers;
using dragonsOfMugloar.Neural;
using NLog;

namespace DragonsOfMugloar.Game
{
    public class Battle
    {
        private NeuronWeb _scaleThickness;
        private NeuronWeb _clawSharpness;
        private NeuronWeb _wingStrength;
        private NeuronWeb _fireBreath;
        private const string StPath = "sT1.txt";
        private const string CsPath = "cS1.txt";
        private const string WsPath = "wS1.txt";
        private const string FbPath = "fB1.txt";
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly StringBuilder sb = new StringBuilder();

        public double TotalWins = 0;
        public double TotalFights = 0;

        public void LoadPerceptrons()
        {
            _scaleThickness = Helper.LoadNeuralNetwork(StPath);
            _clawSharpness = Helper.LoadNeuralNetwork(CsPath);
            _wingStrength = Helper.LoadNeuralNetwork(WsPath);
            _fireBreath = Helper.LoadNeuralNetwork(FbPath);
        }

       
        public  Task<string> NextFightCycle()
        {
            Task<string> task = new Task<string>(() =>
            {
                sb.Clear();
                if (_scaleThickness == null || _clawSharpness == null || _wingStrength == null || _fireBreath == null)
                {
                    log(
                        "Perceprtrons data is not found. Check sT1.txt cS1.txt wS1.txt fB1.txt are present in the application folder.");
                    return sb.ToString();
                }
                
                log("Next game.  " + TotalWins + " wins of " + TotalFights + " fights. " + GetProcent() + " %");
                TotalFights++;
                var d = new Director();
                KnightHero knight = (KnightHero) (d.Construct(new KnightBuilder()));
                if (knight == null)
                {
                    log("Unable to get next knight... abort game cycle.");

                    return sb.ToString();
                }
                Weather weather = (Weather) d.Construct(new WeatherBuilder(knight.GameId));
                if (weather == null)
                {
                    log("Unable to get weather of current batle... abort game cycle.");
                    TotalWins++;
                    return sb.ToString();

                }
                if (weather.Rep.code == "SRO")
                {
                    log("Srtorm. Perhaps knight  die without dragon help.");
                    return sb.ToString();
                }
                log("Knight: " + knight.ToJson());
                log("Weather: " + weather.Rep.ToJson());
                Helper.SetValues(knight, weather, _scaleThickness);
                Helper.SetValues(knight, weather, _clawSharpness);
                Helper.SetValues(knight, weather, _wingStrength);
                Helper.SetValues(knight, weather, _fireBreath);

                var dragon = ConstructDragon();
                log("Dragon ready: " + dragon.ToJson());
                SendDragon(dragon, knight.GameId);
                return sb.ToString();
            });
            task.Start();
            return task;
        }

        private string GetProcent()
        {
          return Math.Round((TotalWins / TotalFights) *100,2).ToString();
        }

        private void log(string text)
        {
            sb.AppendLine(text);
            _log.Info(text);
        }
        private void SendDragon(Dragon dragon, int id)
        {
            _log.Info("Dragon selected: " + dragon.ToJson());

            var responce =
                Request.Put("http://www.dragonsofmugloar.com/api/game/" + id + "/solution", new Rootobject { dragon = dragon }.ToJson());
                    
            _log.Info("Strike responce: " + responce);

            if (!responce.Contains("Defeat"))
            {
                TotalWins++;
                sb.AppendLine("Looks like dragon wins: " + responce);
            }
            else
            {
                sb.AppendLine(":( " + responce);
            }
        }


        private Dragon ConstructDragon()
        {
            int sT1;
            Helper.Selectlevel(_scaleThickness, out sT1);
            int cS1;
            Helper.Selectlevel(_clawSharpness, out cS1);
            int wS1;
            Helper.Selectlevel(_wingStrength, out wS1);
            int fB1;
            Helper.Selectlevel(_fireBreath, out fB1);
            sT1 = (20 - (cS1 + wS1 + fB1));
            Dragon dragon = new Dragon
            {
                scaleThickness = sT1,
                clawSharpness = cS1,
                wingStrength = wS1,
                fireBreath = fB1
            };
            return dragon;
        }
    }
}
