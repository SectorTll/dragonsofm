﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using dragonsOfMugloar.Game;

namespace DragonsOfMugloar.Game
{
   
        public class Knight:IHero
        {
            public string name { get; set; }
            public int attack { get; set; }
            public int armor { get; set; }
            public int agility { get; set; }
            public int endurance { get; set; }
        }

    
}
