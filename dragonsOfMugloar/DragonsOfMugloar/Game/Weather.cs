﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using dragonsOfMugloar.Game;

namespace DragonsOfMugloar.Game
{

    /// <summary>
    /// Ugly class creating by "paste XMl as class" 
    /// </summary>
    public class Weather:IHero
    {
        public  report Rep;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class report
        {

            private string timeField;

            private reportCoords coordsField;

            private string codeField;

            private string messageField;

            private Single varXRatingField;

            /// <remarks/>
            public string time
            {
                get
                {
                    return this.timeField;
                }
                set
                {
                    this.timeField = value;
                }
            }

            /// <remarks/>
            public reportCoords coords
            {
                get
                {
                    return this.coordsField;
                }
                set
                {
                    this.coordsField = value;
                }
            }

            /// <remarks/>
            public string code
            {
                get
                {
                    return this.codeField;
                }
                set
                {
                    this.codeField = value;
                }
            }

            /// <remarks/>
            public string message
            {
                get
                {
                    return this.messageField;
                }
                set
                {
                    this.messageField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("varX-Rating")]
            public Single varXRating
            {
                get
                {
                    return this.varXRatingField;
                }
                set
                {
                    this.varXRatingField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class reportCoords
        {

            private decimal xField;

            private decimal yField;

            private decimal zField;

            /// <remarks/>
            public decimal x
            {
                get
                {
                    return this.xField;
                }
                set
                {
                    this.xField = value;
                }
            }

            /// <remarks/>
            public decimal y
            {
                get
                {
                    return this.yField;
                }
                set
                {
                    this.yField = value;
                }
            }

            /// <remarks/>
            public decimal z
            {
                get
                {
                    return this.zField;
                }
                set
                {
                    this.zField = value;
                }
            }
        }
        
    }

}

