using System;
using System.Threading.Tasks;
using dragonsOfMugloar.Helpers;
using DragonsOfMugloar.Game;
using NLog;

namespace dragonsOfMugloar.Game.Builders
{
    /// <summary>
    /// Base class of Builder pattern
    /// </summary>
    public abstract class Builder
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        public string RawString = "";
        /// <summary>
        /// To get string info of game person
        /// </summary>
        /// <returns></returns>
        internal virtual void GetStringFromWeb()
        {
            RawString = Request.Get(Url);
            if (String.IsNullOrEmpty(RawString))
                _log.Warn("Raw data is null " + ToString());
            _log.Debug("RawData set: " + RawString);    
        }
        public abstract string Url { get;  }

        public abstract IHero GetHeroFromString();
    }
}