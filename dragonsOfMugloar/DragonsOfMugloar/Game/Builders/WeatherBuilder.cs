﻿using System;
using dragonsOfMugloar.Helpers;
using DragonsOfMugloar.Game;
using NLog;

namespace dragonsOfMugloar.Game.Builders
{
    /// <summary>
    /// Weather builder
    /// </summary>
    public class WeatherBuilder : Builder
    {

        private readonly Logger _log = LogManager.GetCurrentClassLogger();
       
        /// <summary>
        /// Ctor with game Id from current battle, used for correct URL creating
        /// </summary>
        /// <param name="gameId"></param>
        public WeatherBuilder(int gameId)
        {
            GameId = gameId.ToString();
        }
        public WeatherBuilder()
        {}

        public override string Url
        {
            get { return "http://www.dragonsofmugloar.com/weather/api/report/" + GameId; }
        }
        /// <summary>
        /// Current battle id, 
        /// </summary>
        public string GameId { get; set; }

        public override IHero GetHeroFromString()
        {
            try
            {
                var rep = RawString.FromXml<Weather.report>();
                return new Weather {Rep = rep};
            }
            catch (Exception ex)//TODO: General exception
            {
                _log.Error("Unable to build Weather from RawString: " + RawString + " Exception: " + ex);

            }
            return null;
        }
    }
}