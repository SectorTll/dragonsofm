﻿using System;
using dragonsOfMugloar.Helpers;
using DragonsOfMugloar.Game;
using NLog;

namespace dragonsOfMugloar.Game.Builders
{
    /// <summary>
    /// Knight builder 
    /// </summary>
    public class KnightBuilder : Builder
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        public override string Url => "http://www.dragonsofmugloar.com/api/game";

        public override IHero GetHeroFromString()
        {
            try
            {
                var knightHero = RawString.FromJson<KnightHero>();
                return knightHero;
            }
            catch (Exception ex)//TODO: remove general exception catching
            {
                _log.Error("Unable to build Knight from RawString: " + RawString +" Exception: "+ex);
            }
            return null;
        }

    }
}