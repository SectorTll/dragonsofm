﻿namespace DragonsOfMugloar.Game
{
    public class KnightHero:IHero
    {
        public int GameId { get; set; }
        public Knight Knight { get; set; }
    }
}