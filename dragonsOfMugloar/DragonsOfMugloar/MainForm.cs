﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DragonsOfMugloar.Game;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Views
{
    public partial class MainForm : Form
    {
        private Battle battle;

        public MainForm()
        {
            InitializeComponent();
            InitLogger();
        }
        public static void InitLogger()
        {
            LoggingConfiguration config = new LoggingConfiguration();
            //          
            var output = new NLog.Targets.DebuggerTarget();
            config.AddTarget("debugger", output);

            var logFile = new FileTarget();
            logFile.CreateDirs = true;
            logFile.ArchiveAboveSize = 1024 * 1024;
            config.AddTarget("file", logFile);

            logFile.FileName = "C:\\Logs\\logging.log";
            logFile.Layout = "${date} | ${level} | ${message}";
            logFile.DeleteOldFileOnStartup = false;
            NLog.LogLevel level = null;
            level = NLog.LogLevel.Trace;

            var rule = new LoggingRule("*", level, logFile);
            //           
            var ruleDebugger = new LoggingRule("*", level, output);
            //          
            config.LoggingRules.Add(rule);
            config.LoggingRules.Add(ruleDebugger);
            LogManager.Configuration = config;
        }

        private void btnFight_Click(object sender, EventArgs e)
        {
             BattleCycle();
        }

        private async void BattleCycle()
        {
            if (battle == null)
            {
                battle = new Battle();
                battle.LoadPerceptrons();

            }
            var t =  battle.NextFightCycle();
            
            txtLogs.Text = await t;
        }
    }
}
