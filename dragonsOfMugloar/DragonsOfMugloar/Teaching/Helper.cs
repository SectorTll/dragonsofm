﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DragonsOfMugloar.Game;

namespace dragonsOfMugloar.Neural
{
    public class Helper
    {
        public static IEnumerable<Dragon> CreateDragonsTypes()
        {
            List<Dragon> dragons=new List<Dragon>();
            for (int i1 = 0; i1 < 11; i1++)
            {
                for (int i2 = 0; i2 < 11; i2++)
                {
                    for (int i3 = 0; i3 < 11; i3++)
                    {
                        for (int i4 = 0; i4 < 11; i4++)
                        {
                            if ((i1+i2+i3+i4)==20)
                            {
                                var dragon = new Dragon();
                                dragon.clawSharpness = i1;
                                dragon.fireBreath = i2;
                                dragon.scaleThickness = i3;
                                dragon.wingStrength = i4;
                                dragons.Add(dragon);
                            }
                        }
                    }
                }
            }
            return dragons;
        }

        public static NeuronWeb CreateNeuralNetwork()
        {

            NeuronWeb web = new NeuronWeb();
            //neurons -attack-10
            //neurons -armor-10
            //neurons -agility-10
            //neurons -endurance-10
            //weather xray 10;
            //weather codes 5;
            var inputlayer =new Layer(66);
            for (int i = 0; i < 10; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "Attack" + i;
            }
            for (int i = 11; i < 22; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "Armour" + i;
            }
            for (int i = 23; i < 34; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "Agility" + i;
            }
            for (int i = 35; i < 46; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "Endurance" + i;
            }
            for (int i = 47; i < 58; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "xRay" + i;
            }
            for (int i = 59; i < 65; i++)
            {
                inputlayer.Neurons.Values.ElementAt(i).Name = "Code" + i;
            }

            
            web.AddLayer(inputlayer);
            var output=new Layer(11);
           
            for (int i = 0; i < 11; i++)
            {
                var neuron = output.Neurons.Values.ElementAt(i);
                neuron.Name = "Level+ " + i;
            }

            web.AddLayer(output);
            return web;
        }

        public static NeuronWeb LoadNeuralNetwork(string path)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase.ToUpperInvariant().TrimEnd(Assembly.GetExecutingAssembly().ManifestModule.Name.ToUpperInvariant().ToCharArray());
            UriBuilder uri = new UriBuilder(codeBase);
            string folder = Uri.UnescapeDataString(uri.Path);
            Directory.CreateDirectory(folder);
            return NeuronWeb.Load(Path.Combine(folder, path));
           
        }
        public static void SaveNeuralNetwork(NeuronWeb web,string path)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase.ToUpperInvariant().TrimEnd(Assembly.GetExecutingAssembly().ManifestModule.Name.ToUpperInvariant().ToCharArray());
            UriBuilder uri = new UriBuilder(codeBase);
            string folder = Uri.UnescapeDataString(uri.Path);
            Directory.CreateDirectory(folder);
            web.Save(Path.Combine(folder, path));

        }

        public static NeuronWeb CreateOrLoadNeuronWeb(string path)
        {
            return LoadNeuralNetwork(path) ?? CreateNeuralNetwork();
        }
        private static Random random= new Random();
        public static double GetRandom()
        {
            return ((double)(random.Next(-100, 100)))/100.0;

        }

        public static void SetValues(KnightHero knight, Weather weather, NeuronWeb web)
        {
            var n = web.Layers[0].Neurons.Values.ToArray();
            foreach (Neuron neuron in n)
            {
                neuron.Thrill = 0;
            }

            n[knight.Knight.attack].Thrill = 1;
            n[knight.Knight.armor + 11].Thrill = 1;
            n[knight.Knight.agility + 11 * 2].Thrill = 1;
            n[knight.Knight.endurance + 11 * 3].Thrill = 1;
            n[(int)weather.Rep.varXRating + 11 * 4].Thrill = 1;


            switch (weather.Rep.code)
            {
                case "NMR":
                    n[59].Thrill = 1;
                    break;
                case "SRO":
                    n[60].Thrill = 1;
                    break;
                case "FUNDEFINEDG":
                    n[61].Thrill = 1;
                    break;
                case "HVA":
                    n[62].Thrill = 1;
                    break;
                case "T E":
                    n[63].Thrill = 1;
                    break;
                default:
                    n[64].Thrill = 1;
                    break;
            }
        }

        /// <summary>
        /// Helper method. get level of appropriate parametr
        /// </summary>
        /// <param name="web"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static Neuron Selectlevel(NeuronWeb web, out int level)
        {
            var n = web.Calculate();
            for (int i = 0; i < web.Layers[1].Neurons.Count; i++)
            {
                if (n == web.Layers[1].Neurons.Values.ElementAt(i))
                {
                    level = i;
                    return n;
                }
            }
            level = -1;
            return null;
        }
    }
}
