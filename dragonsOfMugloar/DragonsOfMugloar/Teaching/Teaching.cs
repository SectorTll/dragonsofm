﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Game.Builders;
using dragonsOfMugloar.Helpers;
using dragonsOfMugloar.Neural;
using DragonsOfMugloar.Game;
using Newtonsoft.Json.Serialization;
using NLog;

namespace dragonsOfMugloar.Teaching
{

    /// <summary>
    /// Methods for teaching neuran networks
    /// </summary>
    public class Teaching//TODO: big ugly class 
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private NeuronWeb _scaleThickness;
        private NeuronWeb _clawSharpness ;
        private NeuronWeb _wingStrength ;
        private NeuronWeb _fireBreath ;
        private int wins2 = 0;

        /// <summary>
        /// Start teching processs
        /// </summary>
        public void StartTeaching()//TODO: exit? split into methods
        {
            Preparations();

            int wins = 0;
            int loss = 0;
            for (int i = 0; i < 10000; i++)
            {
                var d = new Director();
                KnightHero knight = (KnightHero) (d.Construct(new KnightBuilder()));
                _log.Debug("Wins: "+wins+" Loss:" + loss+ " Next knight: " + knight.Knight.ToJson());
                Weather weather = (Weather) d.Construct(new WeatherBuilder(knight.GameId));  
                if (knight == null && weather == null)
                {
                   _log.Warn("knightnor weather is null, abort cycle"); 
                    continue;
                }
                SetValues(knight, weather,_scaleThickness);
                SetValues(knight, weather, _clawSharpness);
                SetValues(knight, weather, _wingStrength);
                SetValues(knight, weather, _fireBreath);

               
                int sT1;
                var sT= Helper.Selectlevel(_scaleThickness, out sT1);
                int cS1;
                var cS = Helper.Selectlevel(_clawSharpness, out cS1);
                int wS1;
                var wS = Helper.Selectlevel(_wingStrength, out wS1);
                int fB1;
                var fB = Helper.Selectlevel(_fireBreath, out fB1);

                Dragon dragon=new Dragon();
                dragon.scaleThickness = sT1;
                dragon.clawSharpness = cS1;
                dragon.wingStrength = wS1;
                dragon.fireBreath = fB1;
                if (weather.Rep.code=="SRO")
                    continue;
               var winner= Strike(dragon,knight.GameId);

                if (winner != null)
                {
                    loss++;

                    if (winner.scaleThickness != sT1)
                    {
                        sT.Teach(NeuronState.Wrong);
                        _scaleThickness.Layers[1].Neurons.Values.ElementAt(winner.scaleThickness).Teach(NeuronState.Right);
                        _log.Debug("Wrong scale Thickness: " + sT1);
                    }
                    if (winner.clawSharpness != cS1)
                    {
                        cS.Teach(NeuronState.Wrong);
                        _clawSharpness.Layers[1].Neurons.Values.ElementAt(winner.clawSharpness).Teach(NeuronState.Right);
                        _log.Debug("Wrong claw Sharpness: " + cS1);
                    }
                    if (winner.wingStrength != wS1)
                    {
                        wS.Teach(NeuronState.Wrong);
                        _wingStrength.Layers[1].Neurons.Values.ElementAt(winner.wingStrength).Teach(NeuronState.Right);
                        _log.Debug("Wrong wind Strength: " + wS1);
                    }
                    if (winner.fireBreath != fB1)
                    {
                        fB.Teach(NeuronState.Wrong);
                        _fireBreath.Layers[1].Neurons.Values.ElementAt(winner.fireBreath).Teach(NeuronState.Right);
                        _log.Debug("Wrong fire breath: " + fB1);
                    }
                }
                else
                {
                    wins++;
                }

                Helper.SaveNeuralNetwork(_scaleThickness,sTPath);
                Helper.SaveNeuralNetwork(_clawSharpness, cSPath);
                Helper.SaveNeuralNetwork(_wingStrength,wSPath);
                Helper.SaveNeuralNetwork(_fireBreath,fBPath);
                
            }
        }
        /// <summary>
        /// Put data of creatted dragon to server and get right dragon in case of fail
        /// </summary>
        /// <param name="dragon"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Dragon Strike(Dragon dragon, int id)
        {
            _log.Info("Dragon selected: " + dragon.ToJson());
            
            var responce =
                Request.Put("http://www.dragonsofmugloar.com/api/game/" + id + "/solution", new Rootobject { dragon = dragon }.ToJson());
                   
            _log.Info("Strike responce: " + responce);

            if (responce.Contains("Defeat"))
            {
               return FindWinner(id, dragon);
            }
            else
            {
                
                _log.Info("Total wins: " + wins2);
                wins2++;
            }
            return null;
        }
        /// <summary>
        /// Set input parametrs for every perceptron
        /// </summary>
        /// <param name="knight"></param>
        /// <param name="weather"></param>
        /// <param name="web"></param>
        private void SetValues(KnightHero knight, Weather weather, NeuronWeb web)
        {
            var n = web.Layers[0].Neurons.Values.ToArray();
            foreach (Neuron neuron in n)
            {
                neuron.Thrill = 0;
            }

            n[knight.Knight.attack].Thrill = 1;
            n[knight.Knight.armor+11].Thrill = 1;
            n[knight.Knight.agility+11*2].Thrill = 1;
            n[knight.Knight.endurance+11*3].Thrill = 1;
            n[(int)weather.Rep.varXRating+11*4].Thrill = 1;


            switch (weather.Rep.code)
            {
                case "NMR":
                    n[59].Thrill = 1;
                    break;
                case "SRO":
                    n[60].Thrill = 1;
                    break;
                case "FUNDEFINEDG":
                    n[61].Thrill = 1;
                    break;
                case "HVA":
                    n[62].Thrill = 1;
                    break;
                case "T E":
                    n[63].Thrill = 1;
                    break;
                default:
                    n[64].Thrill = 1;
                    break;
            }
        }
        /// <summary>
        /// Create four perceptrons for each of four dragons charecte
        /// </summary>
        private void Preparations()
        {
            _scaleThickness = Helper.CreateOrLoadNeuronWeb(sTPath);
            _clawSharpness = Helper.CreateOrLoadNeuronWeb(cSPath);
            _wingStrength = Helper.CreateOrLoadNeuronWeb(wSPath);
            _fireBreath = Helper.CreateOrLoadNeuronWeb(fBPath);
        }
        
        
        private string sTPath="sT1.txt";
        private string cSPath = "cS1.txt";
        private string wSPath = "wS1.txt";
        private string fBPath = "fB1.txt";
        
        /// <summary>
        /// Find correct dragon, which should destroy knight with current weather.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="wrongDragon"></param>
        /// <returns></returns>
        private Dragon FindWinner(int id, Dragon wrongDragon)
        {
            var dragons = Helper.CreateDragonsTypes();

            _log.Info("Find winner");

            Dragon test = wrongDragon.Copy();
            test.scaleThickness = (20 - (wrongDragon.clawSharpness + wrongDragon.fireBreath + wrongDragon.wingStrength));
            if (RequestDragon(id, test, true))
                return test;

            test = wrongDragon.Copy();
            test.clawSharpness = (20 - (wrongDragon.scaleThickness + wrongDragon.fireBreath + wrongDragon.wingStrength));
            if (RequestDragon(id, test, true))
                return test;

            test = wrongDragon.Copy();
            test.fireBreath = (20 - (wrongDragon.clawSharpness + wrongDragon.scaleThickness + wrongDragon.wingStrength));
            if (RequestDragon(id, test, true))
                return test;

            test = wrongDragon.Copy();
            test.wingStrength = (20 - (wrongDragon.clawSharpness + wrongDragon.scaleThickness + wrongDragon.fireBreath));
            if (RequestDragon(id, test, true))
                return test;


            foreach (var dragon in dragons)
            {
               if (RequestDragon(id, dragon, false))
               {
                    _log.Info("Next Smart dragon: " + dragon.ToJson());
                   return dragon;
               }
            }
            _log.Warn("Could not find winner dragon???: ");
            return null;
        }

        private bool RequestDragon(int id, Dragon testDragon, bool isLog)
        {
            var resp =
                Request.Put("http://www.dragonsofmugloar.com/api/game/" + id + "/solution",
                    new Rootobject {dragon = testDragon }.ToJson());
                   

            //if (isLog)
                //_log.Debug(resp);
            if (!resp.Contains("Defeat"))
            {
                if (isLog)
                    _log.Debug(testDragon.ToJson);
                return true;
            }
            return false;
        }
    }
}
