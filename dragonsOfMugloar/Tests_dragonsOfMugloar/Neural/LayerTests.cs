﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar.Neural;
using DragonsOfMugloar;
using NUnit.Framework;
using Views;

namespace Tests_dragonsOfMugloar.Neural
{
    [TestFixture]
    public class LayerTests
    {
        [SetUp]
        public void Init()
        {
            MainForm.InitLogger();
        }

        [Test]
        public void LayerCtorTest()
        {

            Layer layer = new Layer(3);
            Assert.IsTrue(layer.Neurons.Count() == 3);
           
        }
        [Test]
        public void ConnectLayerTest()
        {

            Layer layer = new Layer(3);
            Layer layer2 = new Layer(2);
            layer2.ConnectLayer(layer);

            foreach (var neuron in layer2.Neurons.Values)
            {
                Assert.IsTrue(neuron.Synapses.Count() == 3);
            }
        }
    }

}

