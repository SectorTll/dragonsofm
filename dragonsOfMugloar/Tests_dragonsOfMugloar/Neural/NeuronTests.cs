﻿using System;
using System.Diagnostics;
using System.Linq;
using dragonsOfMugloar.Neural;
using DragonsOfMugloar;
using NUnit.Framework;
using Views;

namespace Tests_dragonsOfMugloar.Neural
{
    [TestFixture]
    public class NeuronTests
    {
        [SetUp]
        public void Init()
        {
            MainForm.InitLogger();
        }
        [Test]
        
        public void AddSynapseTest()
        {
            Neuron neuron =new Neuron();
            Neuron axon =new Neuron();
            neuron.AddSynapse(new Synapse(axon));
        }
        [Test]
        public void AddSynapseSynapceTestException()
        {
            Neuron neuron = new Neuron();
            neuron.Id = Guid.NewGuid();
            Neuron axon = new Neuron();
            axon.Id = Guid.NewGuid();
            var s = new Synapse(axon);
            neuron.AddSynapse(s);
            Assert.Throws<ArgumentOutOfRangeException>(() => neuron.AddSynapse(s));
            Assert.Throws<NullReferenceException>(() => neuron.AddSynapse(null));
        }

        [Test]
        public void ConnectToLayerTest()
        {
            Neuron neuron = new Neuron();
            Layer layer=new Layer(3);
            neuron.ConnectToLayer(layer);
            Assert.IsTrue(neuron.Synapses.Count()==3);
            Layer layer2 = new Layer(5);
            neuron.ConnectToLayer(layer2);
            Assert.IsTrue(neuron.Synapses.Count() == 5);

            foreach (var synapse in neuron.Synapses.Values)
            {
                bool isFound = false;
                foreach (var neuron1 in layer2.Neurons.Values)
                {
                    if (neuron1.Id == ((Synapse)synapse).Axon.Id)
                    {
                        Assert.IsFalse(isFound);
                        Assert.IsTrue(neuron1.Equals(((Synapse)synapse).Axon));
                        isFound = true;
                    }
                }
            }
            neuron.ConnectToLayer(null);
            Assert.IsTrue(neuron.Synapses.Count()==0);


        }
        [Test]
        public void CalculateTest()
        {
            for (double a = 0.01; a < 1; a=a+0.01)
            {
                for (int sum = -100; sum < 100; sum++)
                {
                    var y = 1 / (1 + Math.Exp(-a * sum));
                    if (y>0.8 && y<0.85)
                        System.Diagnostics.Debug.WriteLine("alpha=" +a+" Sum: "+ sum + " sig:" + $"{y:C5}");
                }
                
            }

          

            Neuron neuron = new Neuron();
            Neuron input1 = new Neuron();
            Neuron input2 = new Neuron();
            Neuron input3 = new Neuron();
            Neuron input4 = new Neuron();
            neuron.Id = Guid.NewGuid();
            input1.Id = Guid.NewGuid();
            input2.Id = Guid.NewGuid();
            input3.Id = Guid.NewGuid();
            input4.Id = Guid.NewGuid();

            Synapse s1=new Synapse(input1);
            Synapse s2 = new Synapse(input2);
            Synapse s3 = new Synapse(input3);
            Synapse s4 = new Synapse(input4);
            s1.Weight = 0.5;
            s2.Weight = -0.3;
            s3.Weight = -0.75;
            s4.Weight = -0.20;
            input1.Thrill = 1;
            input2.Thrill = 0;
            input3.Thrill = 1;
            input4.Thrill = 0;

            neuron.AddSynapse(s1);
            neuron.AddSynapse(s2);
            neuron.AddSynapse(s3);
            neuron.AddSynapse(s4);
            var t= neuron.Calculate();

        }
    }
}