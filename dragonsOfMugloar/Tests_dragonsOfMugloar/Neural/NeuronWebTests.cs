﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using DragonsOfMugloar;
using NUnit.Framework;
using Views;

namespace dragonsOfMugloar.Neural.Tests
{
    public class NeuronTests
    {
        [TestFixture]
        public class NeuronWebTests
        {
            [SetUp]
            public void Init()
            {
                MainForm.InitLogger();
            }
            [Test]
            public void CalculateTest()
            {
          
                NeuronWeb web = new NeuronWeb();
                Layer inputLayer = new Layer(2);
                
                Layer output = new Layer(4);
                var n1 = output.Neurons.Values.ElementAt(0);
                var n2 = output.Neurons.Values.ElementAt(1);
                var n3 = output.Neurons.Values.ElementAt(2);
                var n4 = output.Neurons.Values.ElementAt(3);

                web.AddLayer(inputLayer);
                
                web.AddLayer(output);
                Random r=new Random();

//                foreach (var neuron in output.Neurons.Values)
//                {
//                    Assert.IsTrue(neuron.Synapses.Count() == inputLayer.Neurons.Count());
//                }
//               
//                foreach (var neuron in inputLayer.Neurons.Values)
//                {
//                    Assert.IsTrue(neuron.Synapses.Count() == 0);
//                }
                for (int j = 0; j < 10000; j++)
                {

                    if (r.Next(2) == 1)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            inputLayer.Neurons.Values.ElementAt(0).Thrill = 0;
                            inputLayer.Neurons.Values.ElementAt(1).Thrill = 0;
                            var winner = web.Calculate();
                            if (winner == n1)
                            {
                                Debug.WriteLine("n1 "+i);
                                break;
                            }
                            else
                            {
                                winner.Teach(NeuronState.Wrong);
                                n1.Teach(NeuronState.Right);
                                Debug.WriteLine("xxxxx1");
                            }
                        }
                    }
                    if (r.Next(2) == 1)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            inputLayer.Neurons.Values.ElementAt(0).Thrill = 0;
                            inputLayer.Neurons.Values.ElementAt(1).Thrill = 1;
                            var winner = web.Calculate();
                            if (winner == n2)
                            {
                                Debug.WriteLine("n2 " + i);
                                break;
                            }
                            else
                            {
                                winner.Teach(NeuronState.Wrong);
                                n2.Teach(NeuronState.Right);
                                Debug.WriteLine("xxxxx2");
                            }
                        }
                    }
                    if (r.Next(2) == 1)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            inputLayer.Neurons.Values.ElementAt(0).Thrill = 1;
                            inputLayer.Neurons.Values.ElementAt(1).Thrill = 0;
                            var winner = web.Calculate();
                            if (winner == n3)
                            {
                                Debug.WriteLine("n3 " + i);
                                break;
                            }
                            else
                            {
                                winner.Teach(NeuronState.Wrong);
                                n3.Teach(NeuronState.Right);
                                Debug.WriteLine("xxxxx3");
                            }
                        }
                    }
                    if (r.Next(2) == 1)
                    {
                        for (int i = 0; i < 1; i++)
                        {
                            inputLayer.Neurons.Values.ElementAt(0).Thrill = 1;
                            inputLayer.Neurons.Values.ElementAt(1).Thrill = 1;
                            var winner = web.Calculate();
                            if (winner == n4)
                            {
                                Debug.WriteLine("n4 " + i);
                                break;
                            }
                            else
                            {
                                winner.Teach(NeuronState.Wrong);
                                n4.Teach(NeuronState.Right);
                                Debug.WriteLine("xxxxx4");
                            }
                        }
                    }
                    Debug.WriteLine("       ");
                    Debug.WriteLine("       ");
                    Debug.WriteLine("n1  "+n1.Id);
                    Debug.WriteLine(n1.Synapses.Values.ElementAt(0).Weight);
                    Debug.WriteLine(n1.Synapses.Values.ElementAt(1).Weight);
                    Debug.WriteLine("n2  " + n2.Id);
                    Debug.WriteLine(n2.Synapses.Values.ElementAt(0).Weight);
                    Debug.WriteLine(n2.Synapses.Values.ElementAt(1).Weight);
                    Debug.WriteLine("n3  " + n3.Id);
                    Debug.WriteLine(n3.Synapses.Values.ElementAt(0).Weight);
                    Debug.WriteLine(n3.Synapses.Values.ElementAt(1).Weight);
                    Debug.WriteLine("n4  " + n4.Id);
                    Debug.WriteLine(n4.Synapses.Values.ElementAt(0).Weight);
                    Debug.WriteLine(n4.Synapses.Values.ElementAt(1).Weight);
                    Debug.WriteLine("   ------------------------------------------------------------------------------    ");
                    Debug.WriteLine("       ");

                }



            }
            [Test()]
            public void AddLayerTest()
            {
                //Assert.Fail();
            }

            [Test()]
            public void SaveLoadTest()
            {
                NeuronWeb web=new NeuronWeb();
                web.AddLayer(new Layer(10));
                web.AddLayer(new Layer(5));
                web.AddLayer(new Layer(2));

                int weight = 0;
                int weight2 = 0;

                foreach (var layer in web.Layers)
                {
                    foreach (var neuron in layer.Neurons.Values)
                    {
                        foreach (var synapse in neuron.Synapses.Values)
                        {
                            synapse.Weight = weight;
                            weight++;
                        }
                    }
                }

                string codeBase = Assembly.GetExecutingAssembly().CodeBase.ToUpperInvariant().TrimEnd(Assembly.GetExecutingAssembly().ManifestModule.Name.ToUpperInvariant().ToCharArray());
                UriBuilder uri = new UriBuilder(codeBase);
                string folder = Uri.UnescapeDataString(uri.Path);
                Directory.CreateDirectory(folder);
                web.Save(Path.Combine(folder, "web.txt"));
                var newWeb = NeuronWeb.Load(Path.Combine(folder, "web.txt"));
                for (int i = 0; i < 2; i++)
                {
                    Layer l1 = web.Layers[i];
                    Layer l2 = newWeb.Layers[i];
                    Assert.IsTrue(l1.Neurons.Count==l2.Neurons.Count);
                    for (int j = 0; j < l1.Neurons.Count; j++)
                    {
                        Neuron n1 = l1.Neurons.Values.ElementAt(j);
                        Neuron n2 = l2.Neurons.Values.ElementAt(j);
                        Assert.IsTrue(n1.Synapses.Count == n2.Synapses.Count);
                        Assert.IsTrue(n1.Id == n2.Id);
                        for (int k = 0; k < n1.Synapses.Count; k++)
                        {
                            Synapse s1 = n1.Synapses.Values.ElementAt(k);
                            Synapse s2 = n2.Synapses.Values.ElementAt(k);
                            Assert.IsTrue(s1.Axon.Id == s2.Axon.Id);
                            Assert.IsTrue(s1.Weight == s2.Weight);
                            Assert.IsTrue(s1.Weight == weight2);
                            weight2++;

                        }
                    }
                }
            }
        }
    }
}