﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using dragonsOfMugloar;
using dragonsOfMugloar.Game.Builders;
using dragonsOfMugloar.Helpers;
using dragonsOfMugloar.Neural;
using DragonsOfMugloar;
using DragonsOfMugloar.Game;
using NUnit.Framework;
using Views;


namespace Tests_dragonsOfMugloar
{
    [TestFixture]
    public class UnitTest1
    {
        [SetUp]
        public void Init()
        {
            MainForm.InitLogger();
        }
      

        [Test]
        public void TestMethod1()
        {
            NeuronWeb web=new NeuronWeb();
            Layer inputLayer= new Layer(4);
            Layer second = new Layer(3);
            Layer output = new Layer(2);

            web.AddLayer(inputLayer);
            web.AddLayer(second);
            web.AddLayer(output);

            foreach (var neuron in output.Neurons.Values)
            {
                Assert.IsTrue(neuron.Synapses.Count() == second.Neurons.Count());
            }
            foreach (var neuron in second.Neurons.Values)
            {
                Assert.IsTrue(neuron.Synapses.Count() == inputLayer.Neurons.Count());
            }
            foreach (var neuron in inputLayer.Neurons.Values)
            {
                Assert.IsTrue(neuron.Synapses.Count() == 0);
            }

            inputLayer.Neurons.Values.ElementAt(0).Thrill = 0;
            inputLayer.Neurons.Values.ElementAt(1).Thrill = 1;
            inputLayer.Neurons.Values.ElementAt(2).Thrill = 1;
            inputLayer.Neurons.Values.ElementAt(3).Thrill = 1;
            web.Calculate();

        }
        [Test]
        public void CreateKnightWeateherPair_Test()
        {
            for (int i = 0; i < 100; i++)
            {


                var kb = new KnightBuilder();
                string responseString;
                responseString = Request.Get(kb.Url);
                Debug.WriteLine("Knight ResponceString: " + responseString);


                kb.RawString = responseString;
                KnightHero knight = (KnightHero) kb.GetHeroFromString();
                if (knight==null)
                    continue;
                var wb = new WeatherBuilder(knight.GameId);
               

                
               
                    responseString = Request.Get(wb.Url);
                    Debug.WriteLine("Weather ResponceString: " + responseString);

                
                wb.RawString = responseString;
                Weather weather = (Weather) wb.GetHeroFromString();
                if (weather == null)
                    continue;
            }


        }
        [Test]
        public void CreateWeather()
        {
            var kb = new WeatherBuilder();
            string responseString;
            using (var client = new HttpClient())
            {
                responseString = client.GetStringAsync(kb.Url).Result;
                Console.WriteLine("Get ResponceString: " + responseString);
            }

            kb.RawString = responseString;
            var knight = kb.GetHeroFromString();

        }
        [Test]
        public void CreateWeatherFomXml()
        {
            string responceString = "";
            var t= Tests_dragonsOfMugloar.Resource1.XmlWeather;
            var report= JXmlHelper.FromXml<Weather.report>(t);
           

        }
    }
}
