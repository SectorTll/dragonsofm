﻿using NUnit.Framework;
using DragonsOfMugloar.Web;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dragonsOfMugloar;
using dragonsOfMugloar.Game.Builders;
using dragonsOfMugloar.Helpers;
using DragonsOfMugloar.Game;
using Views;

namespace DragonsOfMugloar.Web.Tests
{
    [TestFixture()]
    public class RequestTests
    {

        [SetUp]
        public void Init()
        {
            MainForm.InitLogger();
        }
        [Test()]
        public void PostTest()
        {
            var kb = new KnightBuilder();
            string responseString;
            responseString = Request.Get(kb.Url);
            Debug.WriteLine("Knight ResponceString: " + responseString);


            kb.RawString = responseString;
            KnightHero knight = (KnightHero)kb.GetHeroFromString();
           
            Dragon dragon=new Dragon();
            dragon.clawSharpness = 4;
            dragon.fireBreath = 7;
            dragon.wingStrength = 3;
            dragon.scaleThickness = 6;
            Rootobject r=new Rootobject();
            r.dragon = dragon;
            var json=r.ToJson();

            var responce =Request.Put("http://www.dragonsofmugloar.com/api/game/" + knight.GameId + "/solution", json);
        }
    }
}